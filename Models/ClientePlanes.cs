﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_SECURITAS.Models
{
    public class ClientePlanes
    {

        public int IDPlan { get; set; }
        public int IDCli { get; set; }
        public string Descripcion { get; set; }
        public List<SelectListItem> LCbxClientes { get; set; }
        public List<SelectListItem> LCbxPlanes { get; set; }
        public IEnumerable<ClientePlanes> IClientesPlanes { get; set; }

    }
}