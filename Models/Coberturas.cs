﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APP_SECURITAS.Models
{
    public class Coberturas
    {
        public int IDCob { get; set; }
        public string Descripcion { get; set; }
        public string FechaModificacion { get; set; }
        public IEnumerable<Coberturas> ICoberturas { get; set; }
    }
}