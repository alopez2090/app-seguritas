﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Net;
using System.Web.Helpers;


namespace APP_SECURITAS.Models
{
    public class UtlPlanes
    {
        public List<SelectListItem> FNLLenaCombo(int nTipo)
        {
            string WURL = "";
            if (nTipo == 1)
            {
                WURL = "http://localhost:63299/api/PlanD/FNCbx/";
            }
            else {
                WURL = "http://localhost:63299/api/CobD/FNCbx/";
            }
            
            WebClient client = new WebClient();
            var jsonResult = client.DownloadString(WURL);
            var data = Json.Decode(jsonResult);
            List<SelectListItem> List = new List<SelectListItem>();
            foreach (var x in data)
            {
                List.Add(new SelectListItem()
                {
                    Text = x.Descripcion,
                    Value = nTipo == 1 ? Convert.ToString(x.IDPlan): Convert.ToString(x.IDCob)
                });
            }
            return List;
        }
        public List<SelectListItem> FNComboCli()
        {
            string WURL = "http://localhost:63299/api/ClienteD/FNCbxCli/";
            WebClient client = new WebClient();
            var jsonResult = client.DownloadString(WURL);
            var data = Json.Decode(jsonResult);
            List<SelectListItem> List = new List<SelectListItem>();
            foreach (var x in data)
            {
                List.Add(new SelectListItem()
                {
                    Text = x.Nombre,
                    Value =Convert.ToString(x.IDCli) 
                });
            }
            return List;
        }
        public List<SelectListItem> FNLLenaComboDISP(int nTipo)
        {
            string WURL = "http://localhost:63299/api/CobD/FNCbxDISP?IDPlan=" + nTipo;        
            WebClient client = new WebClient();
            var jsonResult = client.DownloadString(WURL);
            var data = Json.Decode(jsonResult);
            List<SelectListItem> List = new List<SelectListItem>();
            foreach (var x in data)
            {
                List.Add(new SelectListItem()
                {
                    Text = x.Descripcion,
                    //Value = nTipo == 1 ? Convert.ToString(x.IDPlan) : Convert.ToString(x.IDCob)
                    Value = Convert.ToString(x.IDCob)
                });
            }
            return List;
        }

        public List<SelectListItem> FNLLenaPLANDISP(int nTipo)
        {
            string WURL = "http://localhost:63299/api/ClientePlanCB/FNCbxPLANDISP?IDPlan=" + nTipo;
            WebClient client = new WebClient();
            var jsonResult = client.DownloadString(WURL);
            var data = Json.Decode(jsonResult);
            List<SelectListItem> List = new List<SelectListItem>();
            foreach (var x in data)
            {
                List.Add(new SelectListItem()
                {
                    Text = x.Descripcion,
                    Value = Convert.ToString(x.IDPlan) 
                });
            }
            return List;
        }


        public List<PlanesCob> LlenaTabla(int nIDPlanCob)
        {
            List<PlanesCob> oPlanCob = new List<PlanesCob>();
            if (nIDPlanCob == 0)
            {
                
                oPlanCob.Add(new PlanesCob
                {
                    IDPlanCob = 0,
                    Descripcion = ""
                });
            }
            else {
                string WURL = "http://localhost:63299/api/PlanCobD/?nIDPlanCon=" + nIDPlanCob;
                WebClient client = new WebClient();
                var jsonResult = client.DownloadString(WURL);
                var data = Json.Decode(jsonResult);

                foreach (var x in data)
                {
                    oPlanCob.Add(new PlanesCob
                    {
                        IDPlanCob = x.IDPlanCob,
                        Descripcion = x.Descripcion
                    });
                }
            }
            return oPlanCob;
        }
    }
}