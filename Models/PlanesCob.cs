﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_SECURITAS.Models
{
    public class PlanesCob
    {
        public int IDPlan { get; set; }
        public int IDCob { get; set; }
        public int IDPlanCob { get; set; }
        public string Descripcion { get; set; }
        public List<SelectListItem> LCbxCob { get; set; }
        public List<SelectListItem> LCbxPlanes { get; set; }
        public IEnumerable<PlanesCob> IPlanesCob { get; set; }
    }
}