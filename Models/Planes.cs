﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_SECURITAS.Models
{
    public class Planes
    {
        public int IDPlan { get; set; }
        public string Descripcion { get; set; }
        public int IDCob { get; set; }
        public string DescripcionCob { get; set; }
        public string FechaModificacion { get; set; }
        public List<SelectListItem> LCbxCob { get; set; }
        public List<SelectListItem> LCbxPlanes { get; set; }
        public IEnumerable<Planes> IPlanes { get; set; }
    }
}