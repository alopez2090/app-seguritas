﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP_SECURITAS.Models
{
    public class Clientes
    {
        public int IDCli { get; set; }
        public string Nombre  { get; set; }
        public int IDPlan { get; set; }
        public string FechaModificacion { get; set; }
        public List<SelectListItem> LCBXGas { get; set; }
        public IEnumerable<Clientes> IClientes { get; set; }
    }
}