﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;

namespace APP_SECURITAS.Controllers
{
    public class ClientePlanController : Controller
    {
        // GET: ClientePlan
        string WURL = "http://localhost:63299/";
        public ActionResult Index()
        {
            UtlPlanes oUtlPlan = new UtlPlanes();
            ClientePlanes oCliPlan = new ClientePlanes();
            oCliPlan.LCbxClientes = oUtlPlan.FNComboCli();
            oCliPlan.LCbxPlanes = oUtlPlan.FNLLenaCombo(1);            
            return View(oCliPlan);
        }
        public async Task<ActionResult> ConsPlanes(FormCollection oForm) {
            ClientePlanes oCliPlan = new ClientePlanes();
            string WID = oForm["HDcbxCli"].ToString();
            List<ClientePlanes> oClientesPlanInfo = new List<ClientePlanes>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(WURL); client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(string.Format("api/PlanCobD?nIDPlanCon={0}", WID));
                if (Res.IsSuccessStatusCode)
                {
                    var CliResponse = Res.Content.ReadAsStringAsync().Result;
                    oClientesPlanInfo = JsonConvert.DeserializeObject<List<ClientePlanes>>(CliResponse);
                }
                oCliPlan.IClientesPlanes = oClientesPlanInfo;
            }
            UtlPlanes oUtlPlan = new UtlPlanes();
            oCliPlan.LCbxPlanes = oUtlPlan.FNLLenaPLANDISP(int.Parse(WID));
            oCliPlan.LCbxClientes = oUtlPlan.FNComboCli(); oCliPlan.IDCli = int.Parse(WID);
            
            return View("Index", oCliPlan);
        }

        public ActionResult Guardar(FormCollection oForm) {            
            UtlPlanes oUtlPlan = new UtlPlanes();
            ClientePlanes oCliPlan = new ClientePlanes();

            int nIDPlan = int.Parse(oForm["IDPlan"].ToString());
            oCliPlan.IDPlan = nIDPlan;
            oCliPlan.IDCli = int.Parse(oForm["IDCli"].ToString());
            int IDCli = int.Parse(oForm["IDCli"].ToString());


            oCliPlan.LCbxPlanes = oUtlPlan.FNLLenaPLANDISP(nIDPlan);
            oCliPlan.LCbxClientes = oUtlPlan.FNComboCli(); oCliPlan.IDCli = IDCli;

            return View("Index", oCliPlan);
        }

    }
}