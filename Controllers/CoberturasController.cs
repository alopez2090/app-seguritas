﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;

namespace APP_SECURITAS.Controllers
{
    public class CoberturasController : Controller
    {
        // GET: Coberturas
        public ActionResult Index()
        {
            Coberturas oCob = new Coberturas();
            oCob.IDCob = 0; oCob.Descripcion = "";
            return View(oCob);
        }

        [HttpPost]
        public ActionResult Add(Coberturas Cob)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/api/Coberturas");
                var postTAsk = cliente.PostAsJsonAsync<Coberturas>("Coberturas", Cob);
                postTAsk.Wait();
                var Result = postTAsk.Result;
                if (Result.IsSuccessStatusCode)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            ModelState.AddModelError(string.Empty, "Error, contacta al administrador");
            return View(Cob);
        }
        public ActionResult ActCob(int nIDCob)
        {
            Coberturas oCob = null;
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var responseTask = cliente.GetAsync("api/Coberturas/" + nIDCob.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Coberturas>();
                    readTask.Wait();
                    oCob = readTask.Result;
                }
            }
            Coberturas oCobX = new Coberturas();
            oCobX.Descripcion = oCob.Descripcion;
            oCobX.IDCob = oCob.IDCob;
            return View("Index", oCobX);
        }
        [HttpPost]
        public ActionResult Edit(Coberturas oCob)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");

                var putTask = cliente.PutAsJsonAsync($"api/Coberturas/{oCob.IDCob}", oCob);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            return View(oCob);
        }
        public ActionResult DelCob(int nIDCob)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var deleteTask = cliente.DeleteAsync($"api/Coberturas/" + nIDCob.ToString());
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "ListaCob");
                }
            }
            return RedirectToAction("Index", "Inicio");
        }
    }
}