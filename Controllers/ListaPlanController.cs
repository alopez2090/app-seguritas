﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;



namespace APP_SECURITAS.Controllers
{
    public class ListaPlanController : Controller
    {

        string WURL = "http://localhost:63299/";
        public async Task<ActionResult> Index()
        {
            Planes oPlan = new Planes();
            List<Planes> oClientesInfo = new List<Planes>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(WURL);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Planes/");
                if (Res.IsSuccessStatusCode)
                {
                    var CliResponse = Res.Content.ReadAsStringAsync().Result;
                    oClientesInfo = JsonConvert.DeserializeObject<List<Planes>>(CliResponse);
                }
                oPlan.IPlanes = oClientesInfo;
            }
            return View(oPlan);
        }

    }
}