﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;

namespace APP_SECURITAS.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {
            Clientes oCli = new Clientes();
            oCli.Nombre = ""; oCli.IDCli = 0;
            return View(oCli);
        }
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult ActCliente(int nIDCli)
        {
            Clientes oCliente = null;
            using (var cliente = new HttpClient()) {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var responseTask = cliente.GetAsync("api/Cliente/" + nIDCli.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode) {
                    var readTask = result.Content.ReadAsAsync<Clientes>();
                    readTask.Wait();
                    oCliente = readTask.Result;
                }
            }
            Clientes oCli = new Clientes();
            oCli.Nombre = oCliente.Nombre;
            oCli.IDCli = oCliente.IDCli;
            return View("Index", oCli);
        }
        public ActionResult DelCliente(int nIDCli)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var deleteTask = cliente.DeleteAsync($"api/Cliente/" + nIDCli.ToString());
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index","Inicio");
                }
            }
            return RedirectToAction("Index", "Inicio");
        }
        [HttpPost]
        public ActionResult Add(Clientes oCli)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/api/Cliente");
                var postTAsk = cliente.PostAsJsonAsync<Clientes>("Cliente", oCli);
                postTAsk.Wait();
                var Result = postTAsk.Result;
                if (Result.IsSuccessStatusCode)
                {                 
                    return Json("OK", JsonRequestBehavior.AllowGet);

                }
            }
            ModelState.AddModelError(string.Empty, "Error, contacta al administrador");
            return View(oCli);
        }

        [HttpPost]
        public ActionResult Edit(Clientes oCli)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");

                var putTask = cliente.PutAsJsonAsync($"api/Cliente/{oCli.IDCli}", oCli);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            return View(oCli);
        }
    }
}