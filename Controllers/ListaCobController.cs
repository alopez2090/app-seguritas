﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;



namespace APP_SECURITAS.Controllers
{
    public class ListaCobController : Controller
    {
        string WURL = "http://localhost:63299/";
        public async Task<ActionResult> Index()
        {
            Coberturas oCob = new Coberturas();
            List<Coberturas> oClientesInfo = new List<Coberturas>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(WURL);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Coberturas/");
                if (Res.IsSuccessStatusCode)
                {
                    var CliResponse = Res.Content.ReadAsStringAsync().Result;
                    oClientesInfo = JsonConvert.DeserializeObject<List<Coberturas>>(CliResponse);
                }
                oCob.ICoberturas = oClientesInfo;
            }
            return View(oCob);
        }
    }
}