﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;

namespace APP_SECURITAS.Controllers
{
    public class PlanCobController : Controller
    {
        // GET: PlanCob
        string WURL = "http://localhost:63299/";
        public ActionResult Index()
        {
            UtlPlanes oUtlPlan = new UtlPlanes();
            PlanesCob oPlanCob = new PlanesCob();
            oPlanCob.LCbxPlanes = oUtlPlan.FNLLenaCombo(1); oPlanCob.LCbxCob = oUtlPlan.FNLLenaCombo(2);
            oPlanCob.IPlanesCob = oUtlPlan.LlenaTabla(0);
            return View(oPlanCob);
        }        
        public async Task<ActionResult> ConsPlanes(FormCollection oForm)
        {
            PlanesCob oPlan = new PlanesCob();
            string WID = oForm["HDcbxPlan"].ToString();
            List<PlanesCob> oClientesInfo = new List<PlanesCob>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(WURL); client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(string.Format("api/PlanCobD?nIDPlanCon={0}", WID));
                if (Res.IsSuccessStatusCode)
                {
                    var CliResponse = Res.Content.ReadAsStringAsync().Result;
                    oClientesInfo = JsonConvert.DeserializeObject<List<PlanesCob>>(CliResponse);
                }
                oPlan.IPlanesCob = oClientesInfo;
            }
            UtlPlanes oUtlPlan = new UtlPlanes();
            oPlan.LCbxPlanes = oUtlPlan.FNLLenaCombo(1);          
            oPlan.LCbxCob = oUtlPlan.FNLLenaComboDISP(int.Parse(WID));
            oPlan.IDPlan = int.Parse(WID);
            return View("Index",oPlan);
        }

        public ActionResult DelPlanCob(int nIDCob)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var deleteTask = cliente.DeleteAsync($"api/PlanCobD/EliminarPlanCob?id=" + nIDCob.ToString());
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    PlanesCob oPlanCob = new PlanesCob();
                    UtlPlanes oUtlPlan = new UtlPlanes();
                    oPlanCob.LCbxPlanes = oUtlPlan.FNLLenaCombo(1);
                    oPlanCob.LCbxCob = oUtlPlan.FNLLenaCombo(2);
                    oPlanCob.IPlanesCob = oUtlPlan.LlenaTabla(0);
                    return View("Index", oPlanCob);
                }
            }
            return RedirectToAction("Index", "Inicio");
        }

        [HttpPost]
        public ActionResult Guardar(FormCollection oForm) {
            PlanesCob oPlanCob = new PlanesCob();
            int nIDPlan = int.Parse(oForm["IDPlan"].ToString());
            oPlanCob.IDPlan = nIDPlan; 
            oPlanCob.IDCob = int.Parse(oForm["IDCob"].ToString());
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/api/PlanCobD/AddPlanCob");
                var postTAsk = cliente.PostAsJsonAsync<PlanesCob>("PlanCoberturas", oPlanCob);
                postTAsk.Wait();
                var Result = postTAsk.Result;
                if (Result.IsSuccessStatusCode)
                {                    
                    PlanesCob oPlanCobX = new PlanesCob();
                    UtlPlanes oUtlPlan = new UtlPlanes();
                    oPlanCobX.LCbxPlanes = oUtlPlan.FNLLenaCombo(1);
                    oPlanCobX.LCbxCob = oUtlPlan.FNLLenaComboDISP(nIDPlan);
                    oPlanCobX.IDPlan = nIDPlan;
                    oPlanCobX.IPlanesCob = oUtlPlan.LlenaTabla(nIDPlan);
                    return View("Index", oPlanCobX);
                }
            }
            return View("Index");
        }

    }
}



