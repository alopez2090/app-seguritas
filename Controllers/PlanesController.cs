﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;

namespace APP_SECURITAS.Controllers
{
    public class PlanesController : Controller
    {
        // GET: Planes
        public ActionResult Index()
        {
            UtlPlanes oUtlPlan = new UtlPlanes();
            Planes oPlan = new Planes();
            oPlan.IDPlan = 0;
            oPlan.Descripcion = "";
            return View(oPlan);
        }

        public ActionResult ActPlan(int nIDPlan)
        {
            Planes oPlan = null;
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var responseTask = cliente.GetAsync("api/Planes/" + nIDPlan.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Planes>();
                    readTask.Wait();
                    oPlan = readTask.Result;
                }
            }
            Planes oPlanX = new Planes();
            oPlanX.Descripcion = oPlan.Descripcion;
            oPlanX.IDPlan = oPlan.IDPlan;

            return View("Index", oPlanX);
        }


        [HttpPost]
        public ActionResult Add(Planes Plan)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/api/Planes");
                var postTAsk = cliente.PostAsJsonAsync<Planes>("Planes", Plan);
                postTAsk.Wait();
                var Result = postTAsk.Result;
                if (Result.IsSuccessStatusCode)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            ModelState.AddModelError(string.Empty, "Error, contacta al administrador");
            return View(Plan);
        }

        [HttpPost]
        public ActionResult Edit(Planes oPlan)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var putTask = cliente.PutAsJsonAsync($"api/Planes/{oPlan.IDPlan}", oPlan);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            return View(oPlan);
        }

        public ActionResult DelPlan(int nIDPlan)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri("http://localhost:63299/");
                var deleteTask = cliente.DeleteAsync($"api/Planes/" + nIDPlan.ToString());
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "ListaPlan");
                }
            }
            return RedirectToAction("Index", "Inicio");
        }


    }
}