﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using APP_SECURITAS.Models;
using Newtonsoft.Json;


namespace APP_SECURITAS.Views
{
    public class InicioController : Controller
    {
        string WURL = "http://localhost:63299/";
        public async Task<ActionResult> Index()
        {
            Clientes oCli = new Clientes();
            List<Clientes> oClientesInfo = new List<Clientes>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(WURL);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Cliente/");
                if (Res.IsSuccessStatusCode)
                {
                    var CliResponse = Res.Content.ReadAsStringAsync().Result;
                    oClientesInfo = JsonConvert.DeserializeObject<List<Clientes>>(CliResponse);
                }
                oCli.IClientes = oClientesInfo;
            }
            return View(oCli);
        }
    }
}